using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VSX.UniversalVehicleCombat;

public class MyNetworkManager : NetworkManager
{
    public override void OnServerAddPlayer(NetworkConnection conn)
    {
        base.OnServerAddPlayer(conn);

        MyNetworkPlayer player = conn.identity.GetComponent<MyNetworkPlayer>();
        m_listPlayers.Add(player);

        //List<Transform> list = new List<Transform>();
        //
        //Asteroids.Lock();
        //for (int i = 0; i < Asteroids.GetAsteroids().Count; i++)
        //{
        //    list.Add(Asteroids.GetAsteroids()[i].transform);
        //}
        //Asteroids.Unlock();
        //
        //player.SetAsteroids(list);
    }

    public List<MyNetworkPlayer> m_listPlayers = new List<MyNetworkPlayer>();

    //public MassObjectSpawner Asteroids = null;
}