﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyNetworkPlayer : NetworkBehaviour
{
    // az aszteroidák adatai
    [SyncVar]
    private List<Vector3> m_listAsteroidsPosition = null;
    [SyncVar]
    private List<Quaternion> m_listAsteroidsRotation = null;
    [SyncVar]
    private List<Vector3> m_listAsteroidsScale = null;
    [SyncVar]
    private bool m_bIsReadyAsteroids = false;


    // az űrhajó (PlayerShip) adatai
    public GameObject m_SpaceFighterSimplified = null;
    [SyncVar]
    private Vector3 m_v3Position;
    [SyncVar]
    private Quaternion m_quatRotation;
    [SyncVar]
    private Vector3 m_v3Scale;
    [SyncVar]
    private bool m_bIsReadyPlayerShipTransform = false;
    [SyncVar]
    private float m_fShieldCurrentHealth = 0.0f;
    [SyncVar]
    private float m_fArmorCurrentHealth = 0.0f;
    [SyncVar]
    private bool m_bIsReadyCurrentHealth = false;
    [SyncVar]
    private List<string> m_listPrimaryWeapons = null;
    [SyncVar]
    private List<string> m_listSecondaryWeapons = null;
    [SyncVar]
    private bool m_bIsReadyWeapons = false;
    [SyncVar]
    private List<Vector3> m_listPooledObjectsPosition = null;
    [SyncVar]
    private List<Quaternion> m_listPooledObjectsRotate = null;
    [SyncVar]
    private List<string> m_listPooledObjectsNames = null;
    [SyncVar]
    private List<string> m_listPooledObjectsGuids = null;
    [SyncVar]
    private bool m_bIsReadyPooledObjects = false;
    [SyncVar]
    private bool m_bIsStart_FirePrimary = false;
    [SyncVar]
    private bool m_bIsStart_FireSecondary = false;
    [SyncVar]
    private float m_fActionInterval = 0.0f;

    [Server]
    public void SetAsteroids(List<Transform> listAsteroids)
    {
        m_listAsteroidsPosition = new List<Vector3>();
        m_listAsteroidsRotation = new List<Quaternion>();
        m_listAsteroidsScale = new List<Vector3>();

        foreach (Transform transform in listAsteroids)
        {
            m_listAsteroidsPosition.Add(transform.position);
            m_listAsteroidsRotation.Add(transform.rotation);
            m_listAsteroidsScale.Add(transform.localScale);
        }
        m_bIsReadyAsteroids = true;
    }

    public override void OnStartLocalPlayer()
    {
        base.OnStartLocalPlayer();

        Client = this;
    }

    public static MyNetworkPlayer Client = null;

    public bool IsReadyAsteroids()
    {
        return m_bIsReadyAsteroids;
    }

    public List<Vector3> GetAsteroidsPosition()
    {
        return m_listAsteroidsPosition;
    }

    public List<Quaternion> GetAsteroidsRotation()
    {
        return m_listAsteroidsRotation;
    }

    public List<Vector3> GetAsteroidsScale()
    {
        return m_listAsteroidsScale;
    }

    [Server]
    public void Server_SetPlayerShipTransform(Vector3 v3Position, Quaternion quatRotate, Vector3 v3Scale)
    {
        m_v3Position = new Vector3(v3Position.x, v3Position.y, v3Position.z);
        m_quatRotation = new Quaternion(quatRotate.x, quatRotate.y, quatRotate.z, quatRotate.w);
        m_v3Scale = new Vector3(v3Scale.x, v3Scale.y, v3Scale.z);

        m_bIsReadyPlayerShipTransform = true;
    }

    [Command]
    public void Client_SetPlayerShipTransform(Vector3 v3Position, Quaternion quatRotate, Vector3 v3Scale)
    {
        m_v3Position = new Vector3(v3Position.x, v3Position.y, v3Position.z);
        m_quatRotation = new Quaternion(quatRotate.x, quatRotate.y, quatRotate.z, quatRotate.w);
        m_v3Scale = new Vector3(v3Scale.x, v3Scale.y, v3Scale.z);

        //m_bIsReadyPlayerShipTransform = true;
    }

    [Server]
    public void SetCurrentHealth(float fShieldCurrentHealth, float fArmorCurrentHealth)
    {
        m_fShieldCurrentHealth = fShieldCurrentHealth;
        m_fArmorCurrentHealth = fArmorCurrentHealth;

        m_bIsReadyCurrentHealth = true;
    }

    [Server]
    public void SetWeapons(List<string> listPrimaryWeapons, List<string> listSecondaryWeapons)
    {
        m_listPrimaryWeapons = new List<string>();
        m_listSecondaryWeapons = new List<string>();

        foreach (string strName in listPrimaryWeapons)
        {
            m_listPrimaryWeapons.Add(strName);
        }

        foreach (string strName in listSecondaryWeapons)
        {
            m_listSecondaryWeapons.Add(strName);
        }

        m_bIsReadyWeapons = true;
    }

    [Server]
    public void SetPooledObjects(List<Vector3> listPooledObjectsPosition, List<Quaternion> listPooledObjectsRotate, List<string> listPooledObjectsNames, List<string> listPooledObjectsGuids,  bool bIsStart_FirePrimary, bool bIsStart_FireSecondary, float fActionInterval) 
    {
        m_listPooledObjectsPosition = new List<Vector3>();
        m_listPooledObjectsRotate = new List<Quaternion>();
        m_listPooledObjectsNames = new List<string>();
        m_listPooledObjectsGuids = new List<string>();

        foreach (Vector3 p in listPooledObjectsPosition) 
        {
            m_listPooledObjectsPosition.Add(new Vector3(p.x, p.y, p.z));
        }

        foreach (Quaternion q in listPooledObjectsRotate) 
        {
            m_listPooledObjectsRotate.Add(new Quaternion(q.x, q.y, q.z, q.w));
        }

        foreach (string strName in listPooledObjectsNames) 
        {
            m_listPooledObjectsNames.Add(strName);
        }

        foreach (string strGuid in listPooledObjectsGuids)
        {
            m_listPooledObjectsGuids.Add(strGuid);
        }

        m_bIsStart_FirePrimary = bIsStart_FirePrimary;
        m_bIsStart_FireSecondary = bIsStart_FireSecondary;
        m_fActionInterval = fActionInterval;

        m_bIsReadyPooledObjects = true;
    }

    public bool IsReadyPlayerShipTransform()
    {
        return m_bIsReadyPlayerShipTransform;
    }

    public Vector3 GetPosition()
    {
        return m_v3Position;
    }
    public Quaternion GetRotate()
    {
        return m_quatRotation;
    }
    public Vector3 GetScale()
    {
        return m_v3Scale;
    }

    /*
     * private float m_fShieldCurrentHelath = 0.0f;
    private float m_fArmorCurrentHelath = 0.0f;
    private bool m_bIsReadyCurrentHealth = false;
     */
    public float GetShieldCurrentHealth()
    {
        return m_fShieldCurrentHealth;
    }

    public float GetArmorCurrentHealth()
    {
        return m_fArmorCurrentHealth;
    }

    public bool IsReadyCurrentHealth()
    {
        return m_bIsReadyCurrentHealth;
    }

    public List<string> GetPrimaryWeapons()
    {
        return m_listPrimaryWeapons;
    }
    public List<string> GetSecondaryWeapons()
    {
        return m_listSecondaryWeapons;
    }
    public bool IsReadyWeapons() 
    {
        return m_bIsReadyWeapons;
    }

    public List<Vector3> GetPooledObjectsPosition() 
    {
        return m_listPooledObjectsPosition;
    }

    public List<Quaternion> GetPooledObjectsRotate() 
    {
        return m_listPooledObjectsRotate;
    }

    public List<string> GetPooledObjectsNames()
    {
        return m_listPooledObjectsNames;
    }

    public List<string> GetPooledObjectsGuids()
    {
        return m_listPooledObjectsGuids;
    }

    public bool IsStart_FirePrimary() 
    {
        return m_bIsStart_FirePrimary;
    }

    public bool IsStart_FireSecondary()
    {
        return m_bIsStart_FireSecondary;
    }

    public float GetActionInterval() 
    {
        return m_fActionInterval;
    }

    public bool IsReadyPooledObjects() 
    {
        return m_bIsReadyPooledObjects;
    } 
}
