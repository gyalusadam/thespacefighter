﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VSX.FloatingOriginSystem;
using VSX.Pooling;
using VSX.UniversalVehicleCombat;

public class Server : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        m_ShieldDamageable = Shield.GetComponent<Damageable>();
        m_ArmorDamageable = Armor.GetComponent<Damageable>();
    }

    // Update is called once per frame
    void Update()
    {
        // get asteroids
        List<Transform> list = new List<Transform>();
        Asteroids.Lock();
        for (int i = 0; i < Asteroids.GetAsteroids().Count; i++)
        {
            list.Add(Asteroids.GetAsteroids()[i].transform);
        }
        Asteroids.Unlock();

        // get server ship
        Vector3 v3Position = ShipTransform.position;
        Quaternion quatRotate = ShipTransform.rotation;
        Vector3 v3Scale = ShipTransform.localScale;
        float fShieldCurrentHealth = m_ShieldDamageable.CurrentHealth;
        float fArmorCurrentHealth = m_ArmorDamageable.CurrentHealth;

        // weapons
        List<string> listPrimaryWeapons = new List<string>();
        List<string> listSecondaryWeapons = new List<string>();
        foreach (ModuleType element in PrimaryWeaponMount.MountableTypes)
        {
            if (null == element) { continue; }
            listPrimaryWeapons.Add(element.name);
        }
        foreach (ModuleType element in SecondaryWeaponMount.MountableTypes)
        {
            if (null == element) { continue; }
            listSecondaryWeapons.Add(element.name);
        }

        // PooledObjects
        List<Vector3> listPooledObjectsPosition = new List<Vector3>();
        List<Quaternion> listPooledObjectsRotate = new List<Quaternion>();
        List<string> listPooledObjectsNames = new List<string>();
        List<string> listPooledObjectsGuids = new List<string>();

        foreach (ObjectPool objectPool in PoolManager.Instance.ObjectPools) 
        {
            string strName = objectPool.Prefab.name;

            foreach (PooledObject pooledObject in objectPool.ObjectList) 
            {
                if (true == pooledObject.CachedGameObject.activeSelf) 
                {
                    listPooledObjectsPosition.Add(pooledObject.CachedTransform.position);
                    listPooledObjectsRotate.Add(pooledObject.CachedTransform.rotation);
                    listPooledObjectsNames.Add(strName);
                    listPooledObjectsGuids.Add(pooledObject.Guid);
                }
            }
        }

        bool bIsStart_FirePrimary = PoolManager.Instance.m_bIsStart_FirePrimary;
        bool bIsStart_FireSecondary = PoolManager.Instance.m_bIsStart_FireSecondary;
        float fActionInterval = PoolManager.Instance.m_fActionInterval;

        List<MyNetworkPlayer> m_listPlayers = m_MyNetworkManager.m_listPlayers;
        for (int i = 0; i < m_listPlayers.Count; i++)
        {
            if (false == m_listPlayers[i].IsReadyAsteroids())
            {
                m_listPlayers[i].SetAsteroids(list);
            }
            m_listPlayers[i].Server_SetPlayerShipTransform(v3Position, quatRotate, v3Scale);
            m_listPlayers[i].SetCurrentHealth(fShieldCurrentHealth, fArmorCurrentHealth);
            m_listPlayers[i].SetWeapons(listPrimaryWeapons, listSecondaryWeapons);
            m_listPlayers[i].SetPooledObjects(listPooledObjectsPosition, listPooledObjectsRotate, listPooledObjectsNames, listPooledObjectsGuids, bIsStart_FirePrimary, bIsStart_FireSecondary, fActionInterval);
        }

        //PoolManager.Instance.m_bIsStart_FirePrimary = false;
        PoolManager.Instance.m_bIsStart_FireSecondary = false;
    }

    public MyNetworkManager m_MyNetworkManager;
    public Transform ShipTransform;
    public MassObjectSpawner Asteroids = null;

    public GameObject Shield = null; // pajzs
    public GameObject Armor = null; // páncél
    private Damageable m_ShieldDamageable = null;
    private Damageable m_ArmorDamageable = null;

    public ModuleMount PrimaryWeaponMount;
    public ModuleMount SecondaryWeaponMount;
}
