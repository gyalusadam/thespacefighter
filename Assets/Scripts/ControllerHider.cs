using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.Interaction.Toolkit.UI;
using UnityEngine.InputSystem.UI;

public class ControllerHider : MonoBehaviour
{

    

    [SerializeField] GameObject LeftController, RightController;


    void Awake()
    {
        LeftController?.SetActive(false);
        RightController?.SetActive(false);
    }

    public void Menu_Opened()
    {
        LeftController.SetActive(true);
        RightController.SetActive(true);
    }

    public void Menu_Closed()
    {
        LeftController?.SetActive(false);
        RightController?.SetActive(false); 
    }
}
