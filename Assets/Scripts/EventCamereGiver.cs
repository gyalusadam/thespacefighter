using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventCamereGiver : MonoBehaviour
{
    Camera MainCam;
    Canvas canvas;
    private void Awake()
    {
        bool FirstNotWorked = false;
        try
        {
            if (GameObject.FindWithTag("MainCamera") != null )
            {
                MainCam = GameObject.FindWithTag("MainCamera").GetComponent<Camera>();
                canvas = GetComponent<Canvas>();
                canvas.worldCamera = MainCam;
            }
            else
            {
                MainCam = GameObject.Find("Camera").GetComponent<Camera>();
            }
            
        }
        catch (System.Exception exception)
        {
            Debug.LogWarning("Problem with finding the worldCamera in " + gameObject.name + " bc: " + exception);
        }
        
    }
    void Start()
    {
        canvas.worldCamera = MainCam;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
