using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VSX.UniversalVehicleCombat;

public class HealSpawner : MonoBehaviour
{
    [SerializeField] float height = 30;

    [SerializeField] GameObject Prefab;
    [SerializeField] GameObject[] Heals;
    [SerializeField] float HealEnableTimer = 30;

    [SerializeField] MassObjectSpawner asteroids;

    List<GameObject> asteroidobj;

    bool isspawnable = true;


    private void Start()
    {
        asteroidobj = asteroids.GetAsteroids();
        Heals = new GameObject[10];
        int currentHeals = 0;
        for (int i = -20; i < 20; i++)
        {
            for (int z = -35; z < 35; z++)
            {
                if (Random.value >= 0.9f && Random.value >= 0.9f && Random.value >= 0.6f && currentHeals < Heals.Length) 
                {
                    Vector3 pos = new Vector3(i * 2*(Random.value*25), Random.value * height, z * 2 * (Random.value * 50));
                    for (int x = 0; x < asteroidobj.Count; x++)
                    {
                        Vector3 temppos = pos - asteroidobj[x].transform.position;
                        if ((temppos.x < 2 || temppos.x > -2) && (temppos.z < 2 || temppos.z > -2) && (temppos.y < 2 || temppos.y > -2))
                        {
                            isspawnable = false;
                        }
                    }
                    if (isspawnable)
                    {
                        GameObject temp = (GameObject)Instantiate(Prefab, pos, Quaternion.Euler(Vector3.zero), transform);
                        Healer test = temp.GetComponentInChildren<Healer>();
                        test.EnableTime = HealEnableTimer;
                        //Debug.Log(temp.GetComponent<Healer>().EnableTime);
                        Heals[currentHeals] = temp;
                        currentHeals++;
                    }
                    if (Random.value >= 0.5f && currentHeals < Heals.Length && isspawnable)
                    {
                        Vector3 pos2 = new Vector3(i * 100, Random.value * height, z * 100);
                        GameObject temp2 = (GameObject)Instantiate(Prefab, pos2, Quaternion.Euler(Vector3.zero), transform);
                        temp2.GetComponentInChildren<Healer>().EnableTime = HealEnableTimer;
                        Heals[currentHeals] = temp2;
                        currentHeals++;
                    }
                }
                
            }

        }
    }
    

    //Make The the heal object in the map accessable
    public GameObject[] GiveHealObjects()
    {
        return Heals;
    }

    

}
