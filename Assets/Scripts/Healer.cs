using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VSX.UniversalVehicleCombat;
using VSX.UniversalVehicleCombat.Radar;

public class Healer : MonoBehaviour
{
    public bool enabled = true;
    public float EnableTime = 30f;
    float EnableTimer;
    [Space]
    [SerializeField] int HealAmount = 3000;
    [Space]
    public Vector3 Rotation;

    Material OutlineEnable;
    Material InsideEnable;
    [Space]
    [SerializeField] MeshRenderer Plus;
    [Header("Materials:")]
    [SerializeField] Material OutlineDisable;
    [SerializeField] Material InsideDisable;

    MeshRenderer Outline, Inside;
    BoxCollider Collider;

    Trackable Tracker;

    


    private void Awake()
    {
        Outline = gameObject.GetComponent<MeshRenderer>();
        Inside = Plus.GetComponent<MeshRenderer>();
        Collider = gameObject.GetComponent<BoxCollider>();
        Tracker = gameObject.GetComponent<Trackable>();
        EnableTimer = EnableTime;
    }

    void OnTriggerEnter(Collider c)
    {
        
        if (enabled)
        {
            GameObject parent;
            parent = c.transform.parent.gameObject;
            Damageable Healthis;
            Healthis = parent.GetComponentInChildren<Damageable>();
            //Healthis = c.gameObject.GetComponent<Damageable>();
            Healthis.Instaheal(HealAmount);
            
            Disable();
        }

    }


    private void Update()
    {
        transform.Rotate(Rotation);
        if (!enabled)
        {
            EnableTimer -= Time.deltaTime;
            if (EnableTimer <= 0)
            {
                Enable();
            }
        }

    }

    void Disable()
    {
        if (OutlineEnable == null && InsideEnable == null)
        {
            OutlineEnable = Outline.material;
            InsideEnable = Inside.material;
        }
        if (OutlineDisable != null && InsideDisable != null)
        {
            Outline.material = OutlineDisable;
            Inside.material = InsideDisable;
        }
        else
        {
            Debug.LogWarning("Missing Material in heal in gameobject: " + gameObject.name);
        }
        EnableTimer = EnableTime;
        Tracker.activated = false;
        enabled = false;



    }

    void Enable()
    {
        Outline.material = OutlineEnable;
        Inside.material = InsideEnable;
        Tracker.activated = true;
        enabled = true;
        
    }
}
