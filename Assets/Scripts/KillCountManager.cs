using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Collections;
using TMPro;


public class KillCountManager : MonoBehaviour
{
    struct PlayerStat
    {
        public string PlayerName;
        public int KillNum;
        public PlayerStat(string PlayerName, int KillNum)
        {
            this.PlayerName = PlayerName;
            this.KillNum = KillNum;
        }
    }

    [SerializeField] TextMeshProUGUI killCountText;
    [ReadOnly] List<PlayerStat> killCountList = new List<PlayerStat>();


    public int AddPlayer(string playername)
    {
        PlayerStat temp = new PlayerStat();
        temp.PlayerName = playername;
        temp.KillNum = 0;
        killCountList.Add(temp);
        LeaderboardUpdate();
        return killCountList.Count;


    }

    public void AddKillToPlayer(int id)
    {
        int index = id - 1;
        PlayerStat temp = killCountList[index];
        temp.KillNum++;
        killCountList[index] = temp;
        LeaderboardUpdate();
    }

    void LeaderboardUpdate()
    {
        string temp = "Kills:\n";

        foreach (PlayerStat stat in killCountList)
        {
            temp += stat.PlayerName + "    " + stat.KillNum + "\n";
        }
        killCountText.text = temp;
    }

    
}
