using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerID : MonoBehaviour
{
    protected string PlayerName = "ThePlayer";
    public int ID;
    protected KillCountManager Kmanager;
    void Start()
    {
        Kmanager = GameObject.Find("Kill Count Manager").GetComponent<KillCountManager>();
        if (Kmanager != null)
        {
            ID = Kmanager.AddPlayer(PlayerName);
        }

    }

}
