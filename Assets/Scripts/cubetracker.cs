﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using VSX;
using YawVR;

namespace VSX.UniversalVehicleCombat
{
    /// <summary>
        /// Set the type of the object.
        /// </summary>
    public enum ObjectType
    {
        Player,
        Tracker
    };

    public class cubetracker : MonoBehaviour {

        // Use this for initialization
        private Vector3 cubepos;
        private Transform camrot;
        private Quaternion cameravector;
        private float camdiffx;
        private float camdiffy;
        private float camdiffz;
       

        [SerializeField] GameObject Ship;
        Rigidbody ShipRB;
        YawController yawcontroller;



        //   public VehicleCamera vehicleCamera;
        public ObjectType objecttype;
        public ObjectType objectType { get { return objectType; } }
        int rotationDirection = 1;

        private float yaw = 0f;
        private void Awake()
        {
            ShipRB = Ship.GetComponent<Rigidbody>();    
            //yawcontroller = GameObject.FindWithTag("YawTracker").GetComponent<YawController>();
        }

        void FixedUpdate() {


            if (Ship != null)
            {
                if (yawcontroller != null && yawcontroller.connected)
                {
                    camdiffx = ShipRB.transform.InverseTransformDirection(ShipRB.angularVelocity).x;
                    camdiffy = ShipRB.transform.InverseTransformDirection(ShipRB.angularVelocity).y;
                    camdiffz = ShipRB.transform.InverseTransformDirection(ShipRB.angularVelocity).z;

                    switch (objecttype)
                    {
                        case ObjectType.Player:
                            {

                                yaw = -yawcontroller.Device.ActualPosition.yaw;
                                Debug.Log(yaw);
                                Vector3 Headrotation = new Vector3(camdiffx / -20 /*-20*/, yaw, camdiffz / -5);
                                this.transform.localRotation = Quaternion.EulerAngles(Headrotation);


                                break;
                            }
                        case ObjectType.Tracker:
                            {
                                yaw += camdiffy * Time.deltaTime * 57;
                                transform.eulerAngles = new Vector3(camdiffx * 20, yaw, camdiffz * 5);
                                break;
                            }
                        default:
                            break;
                    }
                }
                else
                {
                    try { yawcontroller = GameObject.FindWithTag("YawTracker").GetComponent<YawController>(); }

                    catch (Exception) { }
                }
            }
            else
            {
                try
                {
                    Ship = GameObject.Find("PlayerShip");
                    ShipRB = Ship.GetComponent<Rigidbody>();
                }
                catch (Exception) { }
            }
            
            

        }


    }
}