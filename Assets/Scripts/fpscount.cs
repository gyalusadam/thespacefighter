using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class fpscount : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI fpsDisplay;
    void Update()
    {
        fpsDisplay.text = "FPS: " + ((int)(1f / Time.unscaledDeltaTime)).ToString();
    }
}
