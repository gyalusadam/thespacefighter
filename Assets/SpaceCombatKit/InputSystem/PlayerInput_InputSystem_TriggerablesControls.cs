﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using VSX.Pooling;

namespace VSX.UniversalVehicleCombat
{
    /// <summary>
    /// Input script for controlling the steering and movement of a space fighter vehicle.
    /// </summary>
    public class PlayerInput_InputSystem_TriggerablesControls : VehicleInput
    {

        protected SCKInputAsset input;

        protected TriggerablesManager triggerablesManager;

  
        protected override void Awake()
        {
            base.Awake();

            input = new SCKInputAsset();


            // Fire primary
            input.SpacefighterControls.FirePrimary.performed += ctx => FirePrimary(true);
            input.SpacefighterControls.FirePrimary.canceled += ctx => FirePrimary(false);

            // Fire secondary
            input.SpacefighterControls.FireSecondary.performed += ctx => FireSecondary(true);
            input.SpacefighterControls.FireSecondary.canceled += ctx => FireSecondary(false);


        }

        public void FirePrimary(bool started, bool bPlaySoundOnly = false)
        {
            if (!CanRunInput()) return;


            if (started) 
            {
                PoolManager.Instance.m_bIsStart_FirePrimary = true;
                triggerablesManager.StartTriggeringAtIndex(0, bPlaySoundOnly);
            }
            else 
            {
                PoolManager.Instance.m_bIsStart_FirePrimary = false;
                triggerablesManager.StopTriggeringAtIndex(0);
            }

        }

        public void FireSecondary(bool started, bool bPlaySoundOnly = false)
        {
            if (!CanRunInput()) return;

            if (started)
            {
                PoolManager.Instance.m_bIsStart_FireSecondary = true;
                triggerablesManager.StartTriggeringAtIndex(1, bPlaySoundOnly);
            }
            else 
            {
                PoolManager.Instance.m_bIsStart_FireSecondary = false;
                triggerablesManager.StopTriggeringAtIndex(1);
            }
                
        }

        private void OnEnable()
        {
            input.Enable();
        }

        private void OnDisable()
        {
            input.Disable();
        }

        bool CanRunInput()
        {
            return (initialized && inputEnabled && inputUpdateConditions.ConditionsMet);
        }

        /// <summary>
        /// Initialize this input script with a vehicle.
        /// </summary>
        /// <param name="vehicle">The new vehicle.</param>
        /// <returns>Whether the input script succeeded in initializing.</returns>
        protected override bool Initialize(Vehicle vehicle)
        {

            if (!base.Initialize(vehicle)) return false;

            triggerablesManager = vehicle.GetComponent<TriggerablesManager>();

            if (triggerablesManager == null)
            {
                if (debugInitialization)
                {
                    Debug.Log(GetType().Name + " component failed to initialize, TriggerablesManager component not found on vehicle.");
                }
                return false;
            }
       
            if (debugInitialization)
            {
                Debug.Log(GetType().Name + " component successfully initialized.");
            }

            return true;
        }
    }
}