﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

namespace VSX.UniversalVehicleCombat
{
    /// <summary>
    /// Load a scene using a name or a build index.
    /// </summary>
    public class SceneLoader : MonoBehaviour
    {

        [SerializeField]
        protected string sceneName;

        /// <summary>
        /// Load a scene with the name set in the inspector.
        /// </summary>
        public void LoadScene()
        {
            SceneManager.LoadScene(sceneName);
        }

        /// <summary>
        /// Load a scene with a specified name.
        /// </summary>
        /// <param name="sceneName">The name of the scene.</param>
        public void LoadScene(string sceneName)
        {
            if (sceneName == "SCK_SpaceCombat")
            {
                ScenePass.TargetScene = "SCK_SpaceCombat";
                sceneName = "SCK_Loadout";
            }
            else if (sceneName == "SCK_EnemyWaves")
            {
                ScenePass.TargetScene = "SCK_EnemyWaves";
                sceneName = "SCK_Loadout";
            }
            SceneManager.LoadScene(sceneName);
        }

        /// <summary>
        /// Load a scene with a specified build index.
        /// </summary>
        /// <param name="sceneBuildIndex">The build index of the scene to load.</param>
        public void LoadScene(int sceneBuildIndex)
        {
            
            SceneManager.LoadScene(sceneBuildIndex);
        }


        /// <summary>
        /// Reload the current scene.
        /// </summary>
        public void ReloadActiveScene()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        /// <summary>
        /// Quits the application.
        /// </summary>
        public void QuitApplication()
        {
            Application.Quit();
        }

        public void DropdownSceneLoad()
        {
            TMP_Dropdown Selector = GameObject.Find("Dropdown").GetComponent<TMP_Dropdown>();
            int selected = Selector.value;
            string sceneNameTemp= "not used";
            switch (selected)
            {
                case 1:
                    {
                        sceneNameTemp = "SCK_Loadout";
                        ScenePass.TargetScene = "SCK_SpaceCombat";
                        break;
                    }
                case 2:
                    {
                        sceneNameTemp = "SCK_Loadout";
                        ScenePass.TargetScene = "SCK_EnemyWaves";
                        break;
                    }
                case 3:
                    {
                        sceneNameTemp = "SCK_CapitalShips";
                        break;
                    }
                case 4:
                    {
                        sceneNameTemp = "SCK_MultipleVehicles";
                        break;
                    }
                case 5:
                    {
                        sceneNameTemp = "SCK_Basics_Flying";
                        break;
                    }
                case 6:
                    {
                        //sceneNameTemp = "SCK_Loadout";
                        break;
                    }
                case 7:
                    {
                        //sceneNameTemp = "SCK_Loadout";
                        break;
                    }
                case 8:
                    {
                        //sceneNameTemp = "SCK_Loadout";
                        break;
                    }
                case 9:
                    {
                        //sceneNameTemp = "SCK_Loadout";
                        break;
                    }
                default:
                        break;
            }
            if (sceneNameTemp != "not used")
            {
                sceneName = sceneNameTemp;
                LoadScene();
            }

        }
    }

    public class ScenePass
    {
        public static string TargetScene;
    }

}
