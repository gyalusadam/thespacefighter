using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VSX.Pooling;
using VSX.UniversalVehicleCombat;

public class MyClientController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        // asteroids
        if (null != Asteroid && null != MyNetworkPlayer.Client && true == MyNetworkPlayer.Client.IsReadyAsteroids())
        {
            for (int i = 0; i < MyNetworkPlayer.Client.GetAsteroidsPosition().Count; i++)
            {
                Vector3 v3Position = MyNetworkPlayer.Client.GetAsteroidsPosition()[i];
                Quaternion quatRotate = MyNetworkPlayer.Client.GetAsteroidsRotation()[i];
                Vector3 v3Scale = MyNetworkPlayer.Client.GetAsteroidsScale()[i];

                if (m_listAsteroids.Count <= i)
                {
                    GameObject newAsteroid = Instantiate(Asteroid, v3Position, quatRotate) as GameObject;
                    newAsteroid.transform.localScale = new Vector3(v3Scale.x, v3Scale.y, v3Scale.z);

                    var dot = GameObject.Find("Level");
                    newAsteroid.transform.parent = dot.transform;

                    m_listAsteroids.Add(newAsteroid);
                }

                m_listAsteroids[i].transform.position = v3Position;
                m_listAsteroids[i].transform.rotation = quatRotate;
                m_listAsteroids[i].transform.localScale = v3Scale;
            }
        }

        // ship transform
        if (null != PlayerShipSimplificed && null != MyNetworkPlayer.Client && true == MyNetworkPlayer.Client.IsReadyPlayerShipTransform()) 
        {
            Vector3 v3Position = MyNetworkPlayer.Client.GetPosition();
            Quaternion quatRotate = MyNetworkPlayer.Client.GetRotate();
            Vector3 v3Scale = MyNetworkPlayer.Client.GetScale();

            // create
            if (null == Instance_PlayerShipSimplificed) 
            {
                Instance_PlayerShipSimplificed = Instantiate(PlayerShipSimplificed, v3Position, quatRotate);
                Instance_PlayerShipSimplificed.transform.localScale = v3Scale;

                var dot = GameObject.Find("Level");
                Instance_PlayerShipSimplificed.transform.parent = dot.transform;

                Transform shield = Instance_PlayerShipSimplificed.transform.Find("Ship/Health/Shield");
                m_shieldHealth = shield.GetComponent<Damageable>();

                Transform armor = Instance_PlayerShipSimplificed.transform.Find("Ship/Health/Armor");
                m_shieldArmor = shield.GetComponent<Damageable>();

                Transform primaryWeapon = Instance_PlayerShipSimplificed.transform.Find("Ship/ModuleMounts/PrimaryWeaponMount");
                m_primaryModuleMount = primaryWeapon.GetComponent<ModuleMount>();

                Transform secondaryWeapon = Instance_PlayerShipSimplificed.transform.Find("Ship/ModuleMounts/SecondaryWeaponMount");
                m_secondaryModuleMount = secondaryWeapon.GetComponent<ModuleMount>();
            }

            // update
            Instance_PlayerShipSimplificed.transform.position = v3Position;
            Instance_PlayerShipSimplificed.transform.rotation = quatRotate;
            Instance_PlayerShipSimplificed.transform.localScale = v3Scale;
        }

        // current health
        if (null != PlayerShipSimplificed && null != MyNetworkPlayer.Client && true == MyNetworkPlayer.Client.IsReadyCurrentHealth()) 
        {
            float fShieldHealth = MyNetworkPlayer.Client.GetShieldCurrentHealth();
            float fArmorHealth = MyNetworkPlayer.Client.GetArmorCurrentHealth();

            m_shieldHealth.SetHealth(fShieldHealth);
            m_shieldArmor.SetHealth(fArmorHealth);
        }

        // weapons
        if (null != PlayerShipSimplificed && null != MyNetworkPlayer.Client && true == MyNetworkPlayer.Client.IsReadyWeapons()) 
        {
            List<string> listPrimaryWeapons = MyNetworkPlayer.Client.GetPrimaryWeapons();
            List<string> listSecondaryWeapons = MyNetworkPlayer.Client.GetSecondaryWeapons();

            SetPrimaryWeapons(listPrimaryWeapons);
            SetSecondaryWeapons(listSecondaryWeapons);
        }

        // shots
        if (null != PlayerShipSimplificed && null != MyNetworkPlayer.Client && true == MyNetworkPlayer.Client.IsReadyPooledObjects()) 
        {
            // apply current
            List<Vector3> listPositions = MyNetworkPlayer.Client.GetPooledObjectsPosition();
            List<Quaternion> listRotations = MyNetworkPlayer.Client.GetPooledObjectsRotate();
            List<string> listNames = MyNetworkPlayer.Client.GetPooledObjectsNames();
            List<string> listGuids = MyNetworkPlayer.Client.GetPooledObjectsGuids();
            for (int i = 0; i < listPositions.Count; i++) 
            {
                Vector3 v3Pos = listPositions[i];
                Quaternion quatRotate = listRotations[i];
                string strName = listNames[i];
                string strGuid = listGuids[i];

                PoolManager.Instance.UpdatePrefab(strName, strGuid, v3Pos, quatRotate);
            }

            if (true == MyNetworkPlayer.Client.IsStart_FirePrimary()) 
            {
                PlayerInput.FirePrimary(true, true);   
            }
            if (true == MyNetworkPlayer.Client.IsStart_FireSecondary())
            {
                PlayerInput.FireSecondary(true, true);
            }
        }
    }

    public void SetPrimaryWeapons(List<string> listPrimaryWeapons) 
    {
        // remove if not exists
        for (int i = 0; i < m_primaryModuleMount.MountableTypes.Count; i++)
        {
            ModuleType moduleType = m_primaryModuleMount.MountableTypes[i];

            string strName = moduleType.name;

            if (false == IsContains(strName, listPrimaryWeapons)) 
            {
                m_primaryModuleMount.MountableTypes.RemoveAt(i);
                i--;
            }
        }

        // add if not exists
        foreach (string strName in listPrimaryWeapons) 
        {
            if (false == IsContains(strName, m_primaryModuleMount.MountableTypes)) 
            {
                ModuleType moduleType = GetModuleType(strName);

                if (null != moduleType) 
                {
                    m_primaryModuleMount.MountableTypes.Add(moduleType);
                }
            }
        }
    }

    public void SetSecondaryWeapons(List<string> listSecondaryWeapons)
    {
        // remove if not exists
        for (int i = 0; i < m_secondaryModuleMount.MountableTypes.Count; i++)
        {
            ModuleType moduleType = m_secondaryModuleMount.MountableTypes[i];

            string strName = moduleType.name;

            if (false == IsContains(strName, listSecondaryWeapons))
            {
                m_secondaryModuleMount.MountableTypes.RemoveAt(i);
                i--;
            }
        }

        // add if not exists
        foreach (string strName in listSecondaryWeapons)
        {
            if (false == IsContains(strName, m_secondaryModuleMount.MountableTypes))
            {
                ModuleType moduleType = GetModuleType(strName);

                if (null != moduleType)
                {
                    m_secondaryModuleMount.MountableTypes.Add(moduleType);
                }
            }
        }
    }

    private bool IsContains(string strName, List<ModuleType> list) 
    {
        foreach (ModuleType element in list) 
        {
            if (element.name == strName) 
            {
                return true;
            }
        }

        return false;
    }

    private bool IsContains(string strName, List<string> listPrimaryWeapons)
    {
        foreach (string strCurrentName in listPrimaryWeapons) 
        {
            if (strName == strCurrentName) 
            {
                return true;
            }
        }

        return false;
    }

    private ModuleType GetModuleType(string strName) 
    {
        if ("GunWeapon" == strName) { return GunWeapon; }
        if ("HealthModule" == strName) { return HealthModule; }
        if ("MissileWeapon" == strName) { return MissileWeapon; }
        if ("PowerPlant" == strName) { return PowerPlant; }

        return null;
    }

    public GameObject Asteroid = null;
    public GameObject PlayerShipSimplificed = null;

    private List<GameObject> m_listAsteroids = new List<GameObject>();
    private GameObject Instance_PlayerShipSimplificed = null;
    private Damageable m_shieldHealth;
    private Damageable m_shieldArmor;
    private ModuleMount m_primaryModuleMount;
    private ModuleMount m_secondaryModuleMount;

    public ModuleType GunWeapon;
    public ModuleType HealthModule;
    public ModuleType MissileWeapon;
    public ModuleType PowerPlant;

    public PlayerInput_InputSystem_TriggerablesControls PlayerInput = null; // sound
}
